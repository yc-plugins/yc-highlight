export interface HighlightHandle {
    highlight: (text: string) => void;
    removeHighlight: () => void;
}
declare const Highlight: any;
export default Highlight;
