# 高亮组件

一个 React 组件，用于在其子元素中高亮特定文本。用户可以通过传递自定义类名来定义高亮样式，默认高亮样式为 color: red。

<a href="./README.zh_CN.md">简体中文</a>|<a href="./README.md">English</a>

## Installation

你可以使用 npm 或 Yarn 安装该组件。

### 使用 npm

```bash
npm install yc-highlight
```



### 使用 Yarn

```bash
yarn add yc-highlight
```





## 基本用法

以下是如何在你的 React 应用中使用 Highlight 组件的示例：

你可以传递一个自定义类名给 Highlight 组件，以定义你自己的高亮样式：

```ts
// app.tsx

import React, { useRef } from 'react';
import Highlight, { HighlightHandle } from 'yc-highlight';
import { Input, Button } from 'antd';
import './App.css';

const App: React.FC = () => {
    const [value, setValue] = useState<string>('');
    const highlightRef = useRef<HighlightHandle>(null);

    const handleSearch = () => {
        highlightRef.current?.highlight(value);
    };

    const handleRemoveHighlight = () => {
        highlightRef.current?.removeHighlight();
    };

    return (
        <>
            <div style={{ display: 'flex' }}>
                <Input value={value} onChange={(e) => setValue(e.target.value)} />
                <Button onClick={handleSearch}>Search</Button>
                <Button onClick={handleRemoveHighlight}>Remove Highlight</Button>
            </div>

            <Highlight ref={highlightRef} highlightClassName="custom-highlight">
                <p>This is a sample text to demonstrate the highlight feature.</p>
            </Highlight>
        </>
    );
};

export default App;

```

在你的 App.css 文件中：

```css
// APP.css

.custom-highlight {
    color: red;
    background-color: yellow;
}

```


## API

### Props

- ``` children ``` (React.ReactNode): 要搜索和高亮的子元素。
- ```highlightClassName``` (string, 可选): 自定义高亮样式的类名。默认值为 color: red。

### Methods

- ``` highlight(text: string) ``` : 高亮子元素中的指定文本。

- `removeHighlight()`: 移除子元素中的所有高亮。

  

## License

MIT