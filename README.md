# Highlight Component

A React component to highlight specific text within its child elements. Users can define the highlight style by passing a custom class name, with a default highlight style of `color: red`.

<a href="./README.md">English</a>|<a href="./README.zh_CN.md">简体中文</a>

## Installation

You can install the component using npm or Yarn.

### Using npm

```bash
npm install yc-highlight
```



### USing yarn

```bash
yarn add yc-highlight
```





## Basic Usage

Here’s an example of how to use the Highlight component in your React application:

```ts
// app.tsx

import React, { useRef } from 'react';
import Highlight, { HighlightHandle } from 'yc-highlight';
import { Input, Button } from 'antd';
import './App.css';

const App: React.FC = () => {
    const [value, setValue] = useState<string>('');
    const highlightRef = useRef<HighlightHandle>(null);

    const handleSearch = () => {
        highlightRef.current?.highlight(value);
    };

    const handleRemoveHighlight = () => {
        highlightRef.current?.removeHighlight();
    };

    return (
        <>
            <div style={{ display: 'flex' }}>
                <Input value={value} onChange={(e) => setValue(e.target.value)} />
                <Button onClick={handleSearch}>Search</Button>
                <Button onClick={handleRemoveHighlight}>Remove Highlight</Button>
            </div>

            <Highlight ref={highlightRef} highlightClassName="custom-highlight">
                <p>This is a sample text to demonstrate the highlight feature.</p>
            </Highlight>
        </>
    );
};

export default App;

```


```css
// APP.css

.custom-highlight {
    color: red;
    background-color: yellow;
}

```


## API

### Props

- ``` children ``` (React.ReactNode): The child elements to be searched and highlighted.
- ```highlightClassName``` (string, optional): A custom class name for the highlight style. Default is color: red.

### Methods

- ``` highlight(text: string) ``` : Highlights the specified text within the child elements.

- `removeHighlight()`: Removes all highlights from the child elements.

  

## License

MIT